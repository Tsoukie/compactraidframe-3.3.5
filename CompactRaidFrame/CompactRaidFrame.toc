## Interface: 30300
## Title: Compact Raid Frames
## Author: Tsoukie & Blizzard
## Notes: Blizzard WotLK Classic Raid Frames
## Version: 1.1.51
## SavedVariables: CompactRaidFrameDB
## RequiredDeps: !!!ClassicAPI

embeds.xml
locales.xml

Compat.lua
CUFProfiles\CompactUnitFrameProfiles.xml

FlowContainer.lua
CompactUnitFrame.xml
CompactRaidGroup.xml
CompactPartyFrame.xml

CompactRaidFrames\CompactRaidFrameReservationManager.lua
CompactRaidFrames\CompactRaidFrameContainer.xml
CompactRaidFrames\CompactRaidFrameManager.xml