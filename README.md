# CompactRaidFrame for WotLK (3.3.5a - Private Server)
**`WotLK Classic raid frames, backported for WotLK 3.3.5a private servers.`**

<img src="https://i.imgur.com/xhlQ7fj.jpeg" width="100%">

---

### 📥 [Installation](#-installation-1)
### 📋 [Report Issue](https://gitlab.com/Tsoukie/compactraidframe-3.3.5/-/issues)
### 💬 [FAQ](#-faq-1)
### ❤️ [Support & Credit](#%EF%B8%8F-support-credit-1)

---
### Features:
- **Profiles:** _Save and create profiles for certain situations_

<!-- blank line -->
<br>
<!-- blank line -->

# 📥 Installation

1. Download Latest Release `[.zip, .gz, ...]`:
	- <a href="https://gitlab.com/Tsoukie/compactraidframe-3.3.5/-/releases/permalink/latest" target="_blank">`📥 CompactRaidFrame-3.3.5`</a>
	- <a href="https://gitlab.com/Tsoukie/classicapi/-/releases/permalink/latest" target="_blank">`📥 ClassicAPI`</a> **⚠Required**
2. Extract **both** the downloaded compressed files _(eg. Right-Click -> Extract-All)_.
3. Navigate within each extracted folder(s) looking for the following: `!!!ClassicAPI` or `CompactRaidFrame`.
4. Move folder(s) named `!!!ClassicAPI` or `CompactRaidFrame` to your `Interface\AddOns\` folder.
5. Re-launch game.

<!-- blank line -->
<br>
<!-- blank line -->


# 💬 FAQ

> Why this version over others?

Numerous bugs and issues have been fixed, such as "player" being missing, aura sorting issues and much more.

> Why did you remove built-in features (BigDebuffs, Absorbs) that exist in the other backport?

Not everyone enjoys all these extra features built-in. The plan is to slowly make the extra features more modular. Allowing everyone to pick and choose what features they enjoy.

> Can I enable heal prediction, like retail?

You can download [CompactRaidFrame_HealEx](https://gitlab.com/Tsoukie/compactraidframe_healex) to extend healing information.

> Is Clique supported?

You can grab a modified version [here](https://gitlab.com/Tsoukie/clique-3.3.5).

> Can I use BigDebuffs?

You can download a supported version [here](https://gitlab.com/Tsoukie/bigdebuffs-3.3.5).

> I found a bug!

Please 📋 [report the issue](https://gitlab.com/Tsoukie/compactraidframe-3.3.5/-/issues) with as much detail as possible.

<!-- blank line -->
<br>
<!-- blank line -->


# ❤️ Support & Credit
 
If you wish to show some support you can do so [here](https://streamlabs.com/tsoukielol/tip). Tips are completely voluntary and aren't required to download my projects, however, they are _very_ much appreciated. They allow me to devote more time to creating things I truly enjoy. 💜

<!-- blank line -->
<br>
<!-- blank line -->
  
_This_ version is modified and maintained by [Tsoukie](https://gitlab.com/Tsoukie/) and is **not** related or affiliated with any other version.

### Original AddOn
**Author:** [RomanSpector](https://github.com/RomanSpector/) (not related/affiliated with this version)